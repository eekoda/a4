import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/** Quaternions. Basic operations. */
public class Quaternion {

   private static final double cutOff = 0.000001;

   private double a;
   private double b;
   private double c;
   private double d;

   /** Constructor from four double values.
    * @param a real part
    * @param b imaginary part i
    * @param c imaginary part j
    * @param d imaginary part k
    */
   public Quaternion (double a, double b, double c, double d) {
      this.a = a;
      this.b = b;
      this.c = c;
      this.d = d;
   }

   /** Real part of the quaternion.
    * @return real part
    */
   public double getRpart() {
      return a;
   }

   /** Imaginary part i of the quaternion.
    * @return imaginary part i
    */
   public double getIpart() {
      return b;
   }

   /** Imaginary part j of the quaternion.
    * @return imaginary part j
    */
   public double getJpart() {
      return c;
   }

   /** Imaginary part k of the quaternion.
    * @return imaginary part k
    */
   public double getKpart() {
      return d;
   }

   /** Conversion of the quaternion to the string.
    * @return a string form of this quaternion:
    * "a+bi+cj+dk"
    * (without any brackets)
    */
   @Override
   public String toString() {
      StringBuffer buffer = new StringBuffer();

      if (!compareDouble(a, 0)) {
         buffer.append(a);
      }

      if (!compareDouble(b, 0)) {
         buffer.append(b >= 0.0 ? "+" : "").append(b).append("i");
      }

      if (!compareDouble(c, 0)) {
         buffer.append(c >= 0.0 ? "+" : "").append(c).append("j");
      }

      if (!compareDouble(d, 0)) {
         buffer.append(d >= 0.0 ? "+" : "").append(d).append("k");
      }

      return buffer.toString();
   }


   /** Conversion from the string to the quaternion.
    * Reverse to <code>toString</code> method.
    * @throws IllegalArgumentException if string s does not represent
    *     a quaternion (defined by the <code>toString</code> method)
    * @param s string of form produced by the <code>toString</code> method
    * @return a quaternion represented by string s
    */
   public static Quaternion valueOf (String s) {
      if (s.length() == 0) {
         return new Quaternion(0, 0, 0, 0);
      }

      // Viide: Regex package https://docs.oracle.com/javase/8/docs/api/java/util/regex/package-summary.html
      // Viide: Regex tester https://regex101.com/
      Pattern pattern = Pattern.compile("[-]?\\d+(\\.\\d+)?\\w*");
      Matcher matcher = pattern.matcher(s);

      Double[] values = new Double[]{0., 0., 0., 0.};
      int index = 0;

      while (matcher.find()) {
         String value = matcher.group();

         if (value.endsWith("i")) {
            if (index > 1) {
               throw new RuntimeException("Expected imaginary part i, but found: " + value + " in " + s);
            }

            values[1] = parseNumberWithSuffix(value, "i");
            index = 2;
         } else if (value.endsWith("j")) {
            if (index > 2) {
               throw new RuntimeException("Expected imaginary part j, but found: " + value + " in " + s);
            }

            values[2] = parseNumberWithSuffix(value, "j");
            index = 3;
         } else if (value.endsWith("k")) {
            if (index > 3) {
               throw new RuntimeException("Expected imaginary part k, but found: " + value + " in " + s);
            }

            values[3] = parseNumberWithSuffix(value, "k");
            index = 4;
         } else {
            if (index != 0) {
               throw new RuntimeException("Expected real part, but found: " + value + " in " + s);
            }

            try {
               values[0] = Double.valueOf(value);
               index = 1;
            } catch (NumberFormatException error) {
               throw new RuntimeException("Invalid real part: " + value + " in " + s);
            }
         }
      }

      return new Quaternion(values[0], values[1], values[2], values[3]);
   }

   private static Double parseNumberWithSuffix(String s, String suffix) {
      s = s.replaceFirst(suffix, "");

      try {
         return Double.valueOf(s);
      } catch (NumberFormatException error) {
         throw new NumberFormatException("Invalid imaginary part " + suffix + ": " + s);
      }
   }

   /** Clone of the quaternion.
    * @return independent clone of <code>this</code>
    */
   @Override
   public Object clone() throws CloneNotSupportedException {
      return new Quaternion(a, b, c, d);
   }

   /** Test whether the quaternion is zero.
    * @return true, if the real part and all the imaginary parts are (close to) zero
    */
   public boolean isZero() {
      return compareDouble(a, 0) && compareDouble(b, 0) && compareDouble(c, 0) && compareDouble(d, 0);
   }

   /** Conjugate of the quaternion. Expressed by the formula
    *     conjugate(a+bi+cj+dk) = a-bi-cj-dk
    * @return conjugate of <code>this</code>
    */
   public Quaternion conjugate() {
      return new Quaternion(a, -b, -c, -d);
   }

   /** Opposite of the quaternion. Expressed by the formula
    *    opposite(a+bi+cj+dk) = -a-bi-cj-dk
    * @return quaternion <code>-this</code>
    */
   public Quaternion opposite() {
      return new Quaternion(-a, -b, -c, -d);
   }

   /** Sum of quaternions. Expressed by the formula
    *    (a1+b1i+c1j+d1k) + (a2+b2i+c2j+d2k) = (a1+a2) + (b1+b2)i + (c1+c2)j + (d1+d2)k
    * @param q addend
    * @return quaternion <code>this+q</code>
    */
   public Quaternion plus (Quaternion q) {
      return new Quaternion(a + q.getRpart(), b + q.getIpart(), c + q.getJpart(), d + q.getKpart());
   }

   /** Product of quaternions. Expressed by the formula
    *  (a1+b1i+c1j+d1k) * (a2+b2i+c2j+d2k) = (a1a2-b1b2-c1c2-d1d2) + (a1b2+b1a2+c1d2-d1c2)i +
    *  (a1c2-b1d2+c1a2+d1b2)j + (a1d2+b1c2-c1b2+d1a2)k
    * @param q factor
    * @return quaternion <code>this*q</code>
    */
   public Quaternion times (Quaternion q) {
      return new Quaternion(a * q.a - b * q.b - c * q.c - d * q.d,
              a * q.b + b * q.a + c * q.d - d * q.c,
              a * q.c - b * q.d + c * q.a + d * q.b,
              a * q.d + b * q.c - c * q.b + d * q.a);
   }

   /** Multiplication by a coefficient.
    * @param r coefficient
    * @return quaternion <code>this*r</code>
    */
   public Quaternion times (double r) {
      return new Quaternion(a * r, b * r, c * r, d * r);
   }

   /** Inverse of the quaternion. Expressed by the formula
    *     1/(a+bi+cj+dk) = a/(a*a+b*b+c*c+d*d) +
    *     ((-b)/(a*a+b*b+c*c+d*d))i + ((-c)/(a*a+b*b+c*c+d*d))j + ((-d)/(a*a+b*b+c*c+d*d))k
    * @return quaternion <code>1/this</code>
    */
   public Quaternion inverse() {
      if (isZero()) {
         throw new RuntimeException("Quaterion is 0, cant perform inverse: " + toString());
      }

      return new Quaternion(a / (a * a + b * b + c * c + d * d),
              -b / (a * a + b * b + c * c + d * d),
              -c / (a * a + b * b + c * c + d * d),
              -d / (a * a + b * b + c * c + d * d));
   }

   /** Difference of quaternions. Expressed as addition to the opposite.
    * @param q subtrahend
    * @return quaternion <code>this-q</code>
    */
   public Quaternion minus (Quaternion q) {
      return new Quaternion(a - q.a, b - q.b, c - q.c, d - q.d);
   }

   /** Right quotient of quaternions. Expressed as multiplication to the inverse.
    * @param q (right) divisor
    * @return quaternion <code>this*inverse(q)</code>
    */
   public Quaternion divideByRight (Quaternion q) {
      if (q.isZero()) {
         throw new RuntimeException("Quaterion is 0, cant perform divideByRight on: " + q.toString());
      }

      return times(q.inverse());
   }

   /** Left quotient of quaternions.
    * @param q (left) divisor
    * @return quaternion <code>inverse(q)*this</code>
    */
   public Quaternion divideByLeft (Quaternion q) {
      if (q.isZero()) {
         throw new RuntimeException("Quaterion is 0, cant perform divideByLeft on: " + q.toString());
      }

      return q.inverse().times(this);
   }

   /** Equality test of quaternions. Difference of equal numbers
    *     is (close to) zero.
    * @param qo second quaternion
    * @return logical value of the expression <code>this.equals(qo)</code>
    */
   @Override
   public boolean equals (Object qo) {
      if (!(qo instanceof Quaternion)) {
         return false;
      }

      Quaternion buffer = (Quaternion) qo;

      return compareDouble(buffer.a, a) && compareDouble(buffer.b, b) && compareDouble(buffer.c, c) && compareDouble(buffer.d, d);
   }

   // Viide: Inspiratsioon võetud Loeng 7 (https://echo360.org.uk/lesson/G_635100a3-b90e-43cc-a2df-ff4ad7566316_9006528a-09c7-4033-807b-4a95b8181af2_2020-10-12T12:00:00.000_2020-10-12T13:35:00.000/classroom?focus=Video#sortDirection=desc)
   // aeg ~14:00
   private static boolean compareDouble(double a, double b) {
      return Math.abs(a - b) < cutOff;
   }

   /** Dot product of quaternions. (p*conjugate(q) + q*conjugate(p))/2
    * @param q factor
    * @return dot product of this and q
    */
   public Quaternion dotMult (Quaternion q) {
      Quaternion buffer = q.conjugate().times(this).plus(conjugate().times(q));
      return new Quaternion(buffer.a / 2, buffer.b / 2, buffer.c / 2, buffer.d / 2);
   }

   /** Integer hashCode has to be the same for equal objects.
    * @return hashcode
    */
   @Override
   public int hashCode() {
      return Objects.hash(a, b, c, d);
   }

   /** Norm of the quaternion. Expressed by the formula
    *     norm(a+bi+cj+dk) = Math.sqrt(a*a+b*b+c*c+d*d)
    * @return norm of <code>this</code> (norm is a real number)
    */
   public double norm() {
      return Math.sqrt(a * a + b * b + c * c + d * d);
   }

   /** Main method for testing purposes.
    * @param arg command line parameters
    */
   public static void main (String[] arg) {
      Quaternion arv1 = new Quaternion (-1., 1, 2., -2.);
      if (arg.length > 0)
         arv1 = valueOf (arg[0]);
      System.out.println ("first: " + arv1.toString());
      System.out.println ("real: " + arv1.getRpart());
      System.out.println ("imagi: " + arv1.getIpart());
      System.out.println ("imagj: " + arv1.getJpart());
      System.out.println ("imagk: " + arv1.getKpart());
      System.out.println ("isZero: " + arv1.isZero());
      System.out.println ("conjugate: " + arv1.conjugate());
      System.out.println ("opposite: " + arv1.opposite());
      System.out.println ("hashCode: " + arv1.hashCode());
      Quaternion res = null;
      try {
         res = (Quaternion)arv1.clone();
      } catch (CloneNotSupportedException e) {};
      System.out.println ("clone equals to original: " + res.equals (arv1));
      System.out.println ("clone is not the same object: " + (res!=arv1));
      System.out.println ("hashCode: " + res.hashCode());
      res = valueOf (arv1.toString());
      System.out.println ("string conversion equals to original: "
              + res.equals (arv1));
      Quaternion arv2 = new Quaternion (1., -2.,  -1., 2.);
      if (arg.length > 1)
         arv2 = valueOf (arg[1]);
      System.out.println ("second: " + arv2.toString());
      System.out.println ("hashCode: " + arv2.hashCode());
      System.out.println ("equals: " + arv1.equals (arv2));
      res = arv1.plus (arv2);
      System.out.println ("plus: " + res);
      System.out.println ("times: " + arv1.times (arv2));
      System.out.println ("minus: " + arv1.minus (arv2));
      double mm = arv1.norm();
      System.out.println ("norm: " + mm);
      System.out.println ("inverse: " + arv1.inverse());
      System.out.println ("divideByRight: " + arv1.divideByRight (arv2));
      System.out.println ("divideByLeft: " + arv1.divideByLeft (arv2));
      System.out.println ("dotMult: " + arv1.dotMult (arv2));
   }
}
// end of file
